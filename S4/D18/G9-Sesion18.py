'''
Juego del triqui
tablero 3 * 3
[[' ',' ',' '],[' ',' ',' '],[' ',' ',' ']]
1 2 3
4 5 6
7 8 9
valores X o O
ganador
1=2=3 o 4=5=6 o 7=8=9 o 1=4=7 o 2=5=8 o 3=6=9 o 1=5=9 o 3=5=7
'''

def crearTablero():
    """crea un tablero para jugar triqui
    [[' ',' ',' '],[' ',' ',' '],[' ',' ',' ']]

    Returns:
        Lista de listas: el tablero de juego de 3X3 vacio
    """
    tablero = []
    for i in range(3):
        fila = []
        for j in range(3):
            fila.append(' ')
        tablero.append(fila)
    return tablero

def imprimirTablero(tablero):
    """imprime el tablero de juego, Si la posicion es vacia escribe el numero de la misma


    Args:
        tablero (_type_): el tablero de juego de 3X3 con las jugadas
    """
    contador = 1
    for fila in tablero:
        for columna in fila:
            if columna == " ":
                print(contador, end=" ")
            else:
                print(columna, end=" ")
            contador = contador + 1
        print(" ")

def jugada(tablero, posicion, jugador):
    """rellena una jugada en el tablero para el jugador 

    Args:
        tablero (Lista): el tablero de juego de 3X3
        posicion (int): un valor entre 1 y 9
        jugador (str): X o O
    """
    if posicion == 1:
        tablero[0][0] = jugador
    elif posicion == 2:
        tablero[0][1] = jugador
    elif posicion == 3:
        tablero[0][2] = jugador
    elif posicion == 4:
        tablero[1][0] = jugador
    elif posicion == 5:
        tablero[1][1] = jugador
    elif posicion == 6:
        tablero[1][2] = jugador
    elif posicion == 7:
        tablero[2][0] = jugador
    elif posicion == 8:
        tablero[2][1] = jugador
    elif posicion == 9:
        tablero[2][2] = jugador
    else:
        print("Jugada no valida")

def leerJugada(tablero):
    """imprime el tablero y lee la posicion de la jugada

    Args:
        tablero (lista): el tablero de juego de 3X3

    Returns:
        int: posicion de la jugada
    """
    imprimirTablero(tablero)
    posicion = int(input("Ingrese una posicion para la jugada "))
    return posicion

def existeJugada(tablero):
    """Devuelve verdadero si existen jugadas posibles (posiciones vacias)

    Args:
        tablero (lista): el tablero de juego de 3X3

    Returns:
        bool: true si existe jugada, false si no(esta lleno el tablero)
    """
    existe = False
    for fila in tablero:
        for columna in fila:
            if columna == " ":
                existe = True
                break
    return existe

def siguienteJugador(jugador):
    """Cambia el turno al jugador

    Args:
        jugador (str): puede ser x o O

    Returns:
        str: x si jugador O y O si jugador X
    """
    siguiente="X"
    if jugador == "X":
        siguiente = "O"
    return siguiente

def ganador(tablero):
    """Devuelve verdadero si hay ganador 

    Args:
        tablero (lista): el tablero de juego de 3X3

    Returns:
        bool: hay ganador si un jugador hace alguna de las siguientes combinaciones 1=2=3 o 4=5=6 o 7=8=9 o 1=4=7 o 2=5=8 o 3=6=9 o 1=5=9 o 3=5=7
    """
    resultado = False
    if ((tablero[0][0]== tablero[0][1] and tablero[0][0]== tablero[0][2] and tablero[0][0] != " ")or
        (tablero[1][0]== tablero[1][1] and tablero[1][0]== tablero[1][2] and tablero[1][0] != " ")or
        (tablero[2][0]== tablero[2][1] and tablero[2][0]== tablero[2][2] and tablero[2][0] != " ")or
        (tablero[0][0]== tablero[1][0] and tablero[0][0]== tablero[2][0] and tablero[0][0] != " ")or
        (tablero[0][1]== tablero[1][1] and tablero[0][1]== tablero[2][1] and tablero[0][1] != " ")or
        (tablero[0][2]== tablero[1][2] and tablero[0][2]== tablero[2][2] and tablero[0][2] != " ")or
        (tablero[0][0]== tablero[1][1] and tablero[0][0]== tablero[2][2] and tablero[0][0] != " ")or
        (tablero[0][2]== tablero[1][1] and tablero[0][2]== tablero[2][0] and tablero[0][2] != " ")):
        resultado = True
    return resultado


if __name__=="__main__":
    """Programa princial de triqui
    """
    miTablero = crearTablero()
    jugador ="X"
    while (existeJugada(miTablero)and ganador(miTablero)== False):
        posicion = leerJugada(miTablero)
        jugada(miTablero,posicion,jugador)
        if ganador(miTablero):
            print(f'El jugador {jugador} acaba de ganar la partida')
        jugador = siguienteJugador(jugador)
    imprimirTablero(miTablero)