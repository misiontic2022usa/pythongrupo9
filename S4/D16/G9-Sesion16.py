"""Realizar un programa que capture dos números enteros ingresados por el usuario y 
realice las operaciones aritméticas básicas (suma, resta, multiplicación y división)
"""

#funciones
def suma(a, b) :
    """Suma dos valores

    Args:
        a (int): un numero entero
        b (int): un numero entero

    Returns:
        int: un numero entero
    """
    resultado = a+b
    return resultado

def resta(a,b):
    """resta dos valores

    Args:
        a (int): un numero entero
        b (int): un numero entero

    Returns:
        int: un numero entero
    """
    resultado = a-b
    return resultado

def multiplicacion(a,b):
    """Suma b, a veces

    Args:
        a (int): numero entero 
        b (int): numero entero 

    Returns:
        int: numero entero 
    """
    resultado = 0
    for i in range(a):
        resultado += b
    return resultado

def dividir(a,b):
    """divide a entre b

    Args:
        a (int): numero
        b (int): numero

    Returns:
        float: si es -1 el numero b es 0 y no se puede realizar la operacion
    """
    if b == 0:
        resultado = -1
    else:
        resultado = a/b
    return resultado

def operaciones(numero1, numero2, operacion):
    if operacion == "S":
        total = suma(numero1, numero2)
    elif operacion == "R":
        total = resta(numero1, numero2)
    elif operacion == "M":
        total  =  multiplicacion(numero1, numero2)
    elif operacion == "D":
        total =  dividir(numero1, numero2)
    print("El resultado es ",total)

def menu():
    print("Seleccione la operacion a realizar: ")
    print("(S)uma")
    print("(R)esta")
    print("(M)ultiplicacion")
    print("(D)ividir")
    print("(T)erminar")
    opcion = input()
    return opcion.upper()

# programa principal
if __name__=="__main__":
    numero1 =  int(input("Ingrese un numero "))
    numero2 =  int(input("Ingrese un numero "))
    operacion = menu()
    while operacion!="T":
        if operacion in ['S','R','M','D']:
            operaciones(numero1,numero2,operacion)
        else:
            print("Gracias por usar nuestra calculadora")
        #numero1 =  int(input("Ingrese un numero "))
        #numero2 =  int(input("Ingrese un numero "))
        operacion = menu()

    
