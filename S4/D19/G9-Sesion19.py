import numpy as np

miArreglo = np.array([1,2,3,4,5,6,7,8,9])
print(miArreglo)
print(miArreglo.shape)
print(miArreglo.dtype)
print(miArreglo[4])
print(type(miArreglo))
print(miArreglo[4]+ miArreglo[-1])
print(miArreglo[1:6:2])
print(miArreglo[3:8])
print(miArreglo[:8])
print(miArreglo[::-1])


lista= [1,2,3,4,5,6,7,8]
inicio =  2
fin = 5
while  inicio < fin:
    print(lista[inicio])
    inicio += 1

inicio =  2
fin = 5
for i in range(inicio,fin):
    print(lista[i])

listatmp = lista[inicio:fin]
for dato in listatmp:
    print(dato)