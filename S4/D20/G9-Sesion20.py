'''
Enunciado
Detección de fallas en una línea de producción.

En una fabrica de baldosas de cerámica se producen por día una gran cantidad de este producto. 
Es importante hacer control de calidad sobre el producto. Este control consiste en revisar si en 
un lote de N baldosas hay baldosas defectuosas (puede variar la textura o el color). La solicitud 
del gerente de la planta es que se construya un programa en Python que pueda detectar la cantidad 
de baldosas defectuosas en una de las líneas de producción de la fabrica.

Para detectar si una baldosa es diferente a otra, un sensor escanea las baldosas y si hay alguna 
diferencia, guarda un registro en la memoria. La memoria del sensor esta limitada por la cantidad 
de baldosas que se producen en un intervalo de tiempo determinado.

Entrada	La entrada estará formada por dos líneas:
La primera línea aparecerá dos números N y K que indican el número de baldosas a revisar y el número 
de baldosas que el sensor es capaz de guardar (1≤N≤1000,1≤K≤1000)
La segunda línea contiene M números (entre 1 y 100) separados por espacios que representan las baldosas 
revisadas por el sensor
Las baldosas se consideran defectuosas si están representados por el mismo número
Salida	El programa imprimirá tres números separados por un espacio.

El primero representará el número total de fallas detectadas
El segundo representará la cantidad de fallas detectadas por el sensor considerando que al revisar una 
baldosa solo es capaz de guardar las K baldosas anteriores
El tercero representa la cantidad de baldosas revisadas por el sensor

Entrada:    Linea1 N y K
            Linea2 M datos

Salida:     Una linea con 3 datos
            TotalFallas =  el número total de fallas detectadas (numeros repetidos)
            FallasMemoria = fallas detectadas por el sensor en  las K baldosas anteriores
            Cantidad = la cantidad de baldosas revisadas por el sensor

Proceso:
            Leer Lista1.split()
            Leer Lista2.split()
            N = Lista1[0]
            K = Lista1[1]
            M = Lista2
            TotalFallas = repetidos(M)
            FallasMemoria = memoria(M,K)
            Cantidad = len(Lista2)


            funcion repetidos(lista) ---> TotalFalla
                unico =  []
                TotalFalla = 0
                Para cada Baldosa en lista haga
                    Si Baldosa en unico Entonces
                        TotalFalla += 1
                    FinSi
                    Sino
                        unico agregar (Baldosa)
                    FinSino                
                FinPara
                Devuelva TotalFalla

            funcion memoria(lista, cantidad)
                lista=[1 2 3 1 2] cantidad=3
                Ini Fin Vac         sublista        fallas
                0   1   2       --> [1]
                0   2   3       --> [1 2]
                0   3   1       --> [1 2 3]  ---->  1
                1   4   2       --> [2 3 1]  ---->  1+1
                posicionLista = 1
                Fallas = 0
                longitud =  len(lista)
                Mientras posicionLista < longitud haga
                    inicio = 0
                    Si posicionLista > cantidad Entonces
                        inicio  = posicionLista - cantidad
                    FinSi
                    sublista = lista[inicio:posicionLista]
                    Si lista[posicionLista] esta en sublista
                        Fallas += 1
                    FinSi
                    posicionLista += 1
                FinMientras
                return Fallas



Casos de prueba:

Entrada	Salida Esperada
5 1
1 2 3 1 2	2 0 5
5 2
1 2 3 1 2	2 0 5
5 3
1 2 3 1 2	2 2 5
Dificultad
Instrucciones
Instrucciones para la calificación automática


Cada línea debe contener los valores de los parámetros separados por un espacio.
Es importante no utilizar ningún mensaje a la hora de capturar las entradas, es decir, al utilizar la función input()no agregue ningún texto para capturar los datos.
El resultado "NO DISPONIBLE" siempre debe imprimirse en mayúscula.


'''

'''Definir una función que calcule la longitud de una lista o una cadena dada. 
(Es cierto que python tiene la función len() incorporada, pero escribirla por 
nosotros mismos resulta un muy buen ejercicio.'''



def longitud(cadena):
    lon=0
    for letra in cadena:
        lon += 1
    return lon

print(longitud('abcdef'))


def repetidos(lista):
    setlista = set(lista)
    print(type(setlista), '   ...   ',setlista)
    return len(lista)- len(setlista)


def repetidos2(lista):
    unico =  []
    TotalFalla = 0
    for  Baldosa in lista:
        if Baldosa in unico :
            TotalFalla += 1
        else:
            unico.append(Baldosa)
    return TotalFalla


print(repetidos([1,2,3,1,2,1,3]))
print(repetidos2([1,2,3,1,2,1,3]))



'''
[1,2,3,1,2]
K = 3

Memoria     K   posicion        lista           lV      sublista            Esta en sublista
            3   0               [1,2,3,1,2]     1       []                  Falso
            3   1               [1,2,3,1,2]     2       [1]                 Falso
            3   2               [1,2,3,1,2]     3       [1,2]               Falso
            3   3               [1,2,3,1,2]     1       [1,2,3]             Verdadero
            3   4               [1,2,3,1,2]     2       [2,3,1]             Verdadero


[1,2,3,1,2]
K = 4

Memoria     K   posicion        lista           lV      sublista            lV Esta en sublista
            4   0               [1,2,3,1,2]     1       []                  Falso            
            4   1               [1,2,3,1,2]     2       [1]                 Falso
            4   2               [1,2,3,1,2]     3       [1,2]               Falso
            4   3               [1,2,3,1,2]     1       [1,2,3]             Verdadero
            4   4               [1,2,3,1,2]     2       [1,2,3,1]           Verdadero


funcion memoria(lista, k=len(lista)):
    falla=0
    mapaFallas=[]
    posicion=0
    longitud = len(lista)
    mientras posicion <  longitud haga
        inicio = 0
        fin = posicion
        si posicion > k entonces
            inicio = posicion - k
        sublista =  lista[inico, fin]
        si lista[posicion] esta en  sublista entonces
            falla += 1 
            mapaFalla agregar (1)
        sino
            mapaFalla agregar(0)
        posicion += 1
    devolver Falla, mapaFalla

        
'''


def memoria(lista, k=-1):
    if k==-1:
        k = len(lista)-1 
    falla=0
    mapaFallas=[]
    posicion=0
    longitud = len(lista)
    while posicion <  longitud :
        inicio = posicion+1
        fin = posicion+k+1
        if fin > longitud :
            fin = longitud
        sublista =  lista[inicio: fin]
        if lista[posicion] in  sublista :
            falla += 1 
            mapaFallas.append(1)
        else:
            mapaFallas.append(0)
        posicion += 1
    return falla, mapaFallas


lista1 =  input().split()
lista2 = input().split()
N =  int(lista1[0])
K = int(lista1[1])
M = lista2
print(len(M))
print( memoria(M))
print( memoria(M, K))