""" 
datos = input("Ingrese los datos separados por  espacio ").split()
print(type(datos), datos)

for elemento in datos:
    print(elemento)

i = 0
tam = len(datos)
while i<tam:
    print(datos[i])
    i += 1  """

miDiccionario = {1: "Carlos", 2:"Andres",7:"Maria",4:"Paola", 12:"Carlos"}
print(miDiccionario)


def generarCuadrado( dic :dict, a: int ):
    """genera un diccionario con cuadrados

    Args:
        dic (dict): diccionario a modificar
        a (int): cantidad de datos
    """
    for i in range(a):
        dic[i]= pow(i,2)
    return dic


def llenarDatos(cantidad: int):
    datosEstudiante = {}
    for i in range(cantidad):
        estudiante = []
        nombre = input("Ingrese el nombre ")
        estudiante.append(nombre)
        edad = int(input("Ingrese la edad "))
        estudiante.append(edad)
        curso = input("Ingrese el curso ")
        estudiante.append(curso)
        datosEstudiante[i]= estudiante
    return datosEstudiante


def caracteres(texto: str):
    resultado={}
    for letra in texto:
        resultado[letra.lower()] = texto.lower().count(letra.lower())

    return resultado

cadena = input("Ingrese un texto \n")
datosTexto = caracteres(cadena)
print(datosTexto)


cant = int(input("Ingrese la cantidad de estudiantes "))
datos = llenarDatos(cant)
print(datos)
print(datos.get(3))
# cuadrados
cuadrados = {}
numero = int(input("Ingrese un numero, para generar los cuadrados "))
resultado = generarCuadrado(cuadrados, numero)
print(resultado)
valores =  resultado.values()
print(valores)


# Varias cosas 
